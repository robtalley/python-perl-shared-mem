#!/usr/bin/perl -w
use strict;

use IPC::SysV qw(IPC_PRIVATE S_IRUSR S_IWUSR);
use IPC::SharedMem;
my $mval= 0;
my $mshm= 0;

$mshm = IPC::SharedMem->new(12345678, 11, S_IRUSR);

$mval = $mshm->read(0, 11);

print "$mval\n";
