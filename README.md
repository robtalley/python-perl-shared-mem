# README #

This is my work with shared memory between C, Python, and Perl

### What is this repository for? ###

* This is my work with shared memory between C, Python, and Perl

### How do I get set up? ###

* requires installation of sysv_ipc-0.7.0 which is included in the repo
* Configuration
  * sysv_ipc-0.7.0 has its own build and it worked for me without issue
  * see setup.py
* once installed the repository contains a bunch of examples. 
* see:  http://semanchuk.com/philip/sysv_ipc/ for docs on the library
* the tests and demos in sysv_ipc-0.7.0 are between C and Python.
	I had to pick up the Perl parts from elsewhere

The two programs that demostrate shared mem between python and perl
are:

shmcreator.pl

This creates a new shm segment, writes a message to it, sleeps, and then
reads it back, then cleans up removing the segment. 

shmreader.py
This aquires the shared memory segment, reads it and prints the contents, 
then wrotes a value back to it, then exits. 

Run shmcreator.pl first in one window
Then run shmreader.py in another


This is a solution I plan to implement on the Hercules since I have a 
significant base of perl code and I have some sensors that the code for
only supports Python. At this time I'm not interested in porting either to
the other so I will have the procesess communicate via IPC shared memory. 

This was put together from several sources, it's certainly not original . 

