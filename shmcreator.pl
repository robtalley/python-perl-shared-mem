#!/usr/bin/perl -w

#$IPC_PRIVATE = 0;
$IPC_RMID = 0;
$key = 123456;
$IPC_CREAT = 0001000;
$size = 2000;
#$key = shmget($IPC_PRIVATE, $size , 0777 | $IPC_CREAT );
$mykey = shmget($key, $size , 0777 | $IPC_CREAT );
die unless defined $mykey;
$message = "this is My Message #1";
shmwrite($mykey, $message, 0, 60 ) || die "$!";
shmread($mykey,$buff,0,60) || die "$!";
print $buff,"\n";
sleep(30);
shmread($mykey,$buff,0,60) || die "$!";
print $buff,"\n";
print "deleting $mykey\n";
shmctl($mykey ,$IPC_RMID, 0) || die "$!";
